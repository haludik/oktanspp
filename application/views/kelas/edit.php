<div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Kelas <strong><?php echo $edit->nama; ?></strong></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open('kelas/edit'); ?>
              <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
              <div class="box-body">

                <?php if(validation_errors() != false) { ?>
                  <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo validation_errors(); ?>
                </div>
                <?php } ?>

                <div class="form-group">
                  <label for="nama">Kelas</label>
                  <input type="text" name="nama" id="nama" class="form-control" placeholder="Kelas" value="<?php echo $edit->nama; ?>">
                </div>

                <div class="form-group">
                  <label for="jurusan">Jurusan</label>
                  <input type="text" name="jurusan" id="jurusan" class="form-control" placeholder="Jurusan" value="<?php echo $edit->jurusan; ?>">
                </div>
                
              </div>

              <div class="box-footer">
              <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
                <button type="submit" class="btn btn-primary" name="submit" value="submit">Update</button>
              </div>
            <?php echo form_close(); ?>
          </div>
    

        </div>
      </div>
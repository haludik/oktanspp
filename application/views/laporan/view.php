<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">
<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>

<script>
    $(function() {
        //Initialize Select2 Elements
        $(".select2").select2();
    });
</script>

<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Generate Laporan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->


            <form action="<?php echo base_url(); ?>/../laporan/generate" method="post" target="_blank">
                <div class="box-body">

                    <?php if (validation_errors() != false) { ?>
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <?php echo validation_errors(); ?>
                        </div>
                    <?php } ?>

                    <div class="form-group">
                        <label for="NISN">NISN</label>
                        <input type="text" name="id" id="id" class="form-control" placeholder="NISN" value="">
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama Lengkap</label>
                        <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" value="">
                    </div>

                    <div class="form-group">
                        <label for="kelas">Kelas</label>
                        <?php echo form_dropdown('kelas', array_merge(array('- Semua Kelas -' => '- Semua Kelas -'), $kelas), '- Semua Kelas -'); ?>
                    </div>

                    <div class="form-group">
                        <label for="jurusan">Jurusan</label>
                        <?php echo form_dropdown('jurusan', array_merge(array('- Semua Jurusan -' => '- Semua Jurusan -'), $jurusan), '- Semua Jurusan -'); ?>
                    </div>
                    
                    <div class="form-group">
                        <label for="tahun">Tahun dibayar</label>
                        <?php echo form_dropdown('tahun', array_merge(array('- Semua Tahun -' => '- Semua Tahun -'), $tahun), '- Semua Tahun -'); ?>
                    </div>

                    <div class="form-group">
                        <label for="bulan_dibayar">Bulan dibayar</label>
                        <?php echo form_dropdown('bulan_dibayar', array_merge(array('- Semua Bulan -' => '- Semua Bulan -'), $bulan), '- Semua Bulan -'); ?>
                    </div>
                    <div class="form-group">
                        <label for="petugas">Petugas</label>
                        <?php echo form_dropdown('petugas', array_merge(array('- Semua Petugas -' => '- Semua Petugas -'), $petugas), '- Semua Petugas -'); ?>
                    </div>

                    <div class="form-group">
                        <label for="status">Status</label>
                        <?php echo form_dropdown('status', array_merge(array('- Semua Status -' => '- Semua Status -'), $status), '- Semua Status -'); ?>
                    </div>
                </div>


                <!-- /.box-body -->

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" name="submit" value="submit">Generate Laporan</button>
                </div>
            </form>
        </div>
        <!-- /.box -->

        <!-- Form Element sizes -->

        <!-- /.box -->


        <!-- /.box -->

        <!-- Input addon -->

        <!-- /.box -->

    </div>
    <!--/.col (left) -->
    <!-- right column -->

    <!--/.col (right) -->
</div>
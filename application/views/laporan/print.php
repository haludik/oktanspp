<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Laporan Pembayaran SPP</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.css">

<!-- DataTables -->
<script src="<?= base_url() ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url() ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
    $(function() {
        $("#example1").DataTable();
    });
</script>
</head>

<body class="hold-transition login-page">



    <div class="box">
        <div class="box-header">
            <h3 class="box-title">
                <h1>LAPORAN SPP</h1><b> <?php
                                        echo '<br> NISN: ' . $nisn;
                                        echo '<br> Nama: ' . $nama;
                                        echo '<br> Kelas: ' . $kelas;
                                        echo '<br> Jurusan: ' . $jurusan;
                                        echo '<br> Tahun Dibayar: ' . $tahun;
                                        echo '<br> Bulan Dibayar: ' . $bulan_dibayar;
                                        echo '<br> Petugas: ' . $petugas;
                                        echo '<br> Status: ' . $status;
                                        ?></b>
            </h3>
        </div>

        <br><br>
        <!-- /.box-header -->
        <div class="box-body">


            <?php if ($this->session->flashdata('info')) { ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <?php echo $this->session->flashdata('info'); ?>
                </div>
            <?php } ?>


            <?php

            if ($hasdata) {

            ?>

                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Tahun Dibayar</th>
                            <th>Bulan Dibayar</th>
                            <th>NISN</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Jurusan</th>                          
                            <th>Tanggal Dibayar</th>
                            <th>Petugas</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $keterangan = "";
                        $no = 1;
                        foreach ($laporan as $row) {


                            if ($row->DetailStatus == "Lebih ") {
                                $keterangan = $row->Status .' (' . $row->DetailStatus . " " . (string)($row->jml_bayar - $row->nominalspp) . ')';
                            } else if ($row->DetailStatus == "Kurang ") {
                                $keterangan = $row->Status . " (" . $row->DetailStatus . " " . (string)($row->nominalspp - $row->jml_bayar) . ")";
                            }
                            else 
                            {
                                $keterangan = $row->DetailStatus;
                            }
                        ?>
                            <tr>
                                <td><?php echo $no; ?></td>
                                <td><?php echo $row->tahun; ?></td>
                                <td><?php echo $row->bulan; ?></td>
                                <td><?php echo $row->nisn; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->kelas; ?></td>
                                <td><?php echo $row->jurusan; ?></td>                              
                                <td><?php echo $row->tgl_bayar; ?></td>
                                <td><?php echo $row->username; ?></td>
                                <td><?php echo $keterangan ?></td>

                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Tahun Dibayar</th>
                            <th>Bulan Dibayar</th>
                            <th>NISN</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Jurusan</th>
                            <th>Tanggal Dibayar</th>
                            <th>Petugas</th>
                            <th>Status</th>
                        </tr>
                    </tfoot>
                </table>
        </div>

    <?php } else {
                echo "<center>Data tidak ditemukan</center>";
            } ?>
    <!-- /.box-body -->
    <br><br>
    <h4> Kembali ke halaman utama <a href="<?= base_url() ?>site">Klik Disini</a> </h4>
    </div>
</body>

</html>
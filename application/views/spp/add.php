<!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datepicker/datepicker3.css">
  <!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

  <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Spp</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open_multipart('spp/add'); ?>

              <div class="box-body">

                <?php if(validation_errors() != false) { ?>
                  <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo validation_errors(); ?>
                </div>
                <?php } ?>

                <div class="form-group">
                  <label for="merk">No.Spp</label>
                  <input type="number" name="spp" id="spp" class="form-control" placeholder="No.Spp" value="<?php echo set_value('id'); ?>">
                </div>
              <div class="form-group">
                  <label for="merk">Tahun Ajaran</label>
                  <input type="text" name="tahun" id="tahun" class="form-control" placeholder="Tahun Ajaran" 
                  value="<?php echo set_value('tahun'); ?>">
                </div>
              <div class="form-group">
                  <label for="merk">Nominal (Rp.)</label>
                  <input type="text" name="nominal" id="nominal" class="form-control" placeholder="Nominal" value="<?php echo set_value('nominal'); ?>">
                </div>

              <div class="box-footer">
              <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
                <button type="submit" class="btn btn-primary" name="submit" value="submit">Save</button>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
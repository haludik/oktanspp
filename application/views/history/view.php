<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login Administrator</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url()?>assets/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">

<link rel="stylesheet" href="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.css">

<!-- DataTables -->
<script src="<?=base_url()?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>


<div class="box">
            <div class="box-header">
              <h3 class="box-title"><h1>Data Pembayaran</h1><b> <?php  if(isset($_SESSION['nisn']) && isset($_SESSION['namasiswa']) && isset($_SESSION['kelas']) && isset($_SESSION['jurusan']))
              {
                echo '<br> NISN: '. $_SESSION['nisn'];
                echo '<br> Nama: '. $_SESSION['namasiswa'];
                echo '<br> Kelas: '. $_SESSION['kelas'];
                echo '<br> Jurusan: '. $_SESSION['jurusan'];
              }?></b></h3>
            </div>

            <br><br>
            <!-- /.box-header -->
            <div class="box-body">

          
            <?php if($this->session->flashdata('info')) { ?>
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <?php echo $this->session->flashdata('info'); ?>
              </div>
            <?php } ?>

              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>No. Pembayaran</th>
                  <th>NISN</th>
                  <th>Bulan Dibayar</th>
                  <th>No. SPP</th>
                  <th>Jumlah Bayar</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $no = 1;
                  foreach($transaksi as $row) {
                  ?>         
                    <tr>
                      <td><?php echo $no; ?></td>
                      <td><?php echo $row->id_pembayaran; ?></td>
                      <td><?php echo $row->nisn; ?></td>
                      <td><?php echo $row->bulan_dibayar; ?></td>
                      <td><?php echo $row->id_spp; ?></td>
                      <td><?php echo $row->jml_bayar; ?></td>

                    </tr>
                <?php
                  $no++; }
                ?> 
                </tbody>
                <tfoot>
                <tr>
                 <th>No.</th>
                  <th>No. Pembayaran</th>
                  <th>NISN</th>
                  <th>Bulan Dibayar</th>
                  <th>No. SPP</th>
                  <th>Jumlah Bayar</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
            <br><br>
          <h4> Kembali ke halaman pencarian history pembayaran siswa <a href="<?=base_url()?>">Klik Disini</a> </h4>
          </div>
          </body>
</html>

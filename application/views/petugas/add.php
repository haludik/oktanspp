<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Data Petugas</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->

      <?php echo form_open('petugas/add'); ?>

      <div class="box-body">

        <?php if (validation_errors() != false) { ?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo validation_errors(); ?>
          </div>
        <?php } ?>

        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" name="username" id="username" class="form-control" id="username" placeholder="Username Petugas">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" name="password" id="password" class="form-control" id="password" placeholder="Password Petugas">
        </div>
        <div class="form-group">
          <label for="nama">Nama Petugas</label>
          <input type="text" name="nama" id="nama" class="form-control" id="nama" placeholder="Nama Lengkap Petugas">
        </div>
        <div class="form-group">
          <label for="level">Level Petugas</label>
          <?php echo form_dropdown('level', $level, "Petugas"); ?>
        </div>


      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
        <button type="submit" class="btn btn-primary" name="submit" value="submit">Save</button>
      </div>
      <?php echo form_close(); ?>
    </div>
    <!-- /.box -->

    <!-- Form Element sizes -->

    <!-- /.box -->


    <!-- /.box -->

    <!-- Input addon -->

    <!-- /.box -->

  </div>
  <!--/.col (left) -->
  <!-- right column -->

  <!--/.col (right) -->
</div>
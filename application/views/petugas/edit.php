<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.min.css">
<!-- Select2 -->
<script src="<?= base_url() ?>assets/plugins/select2/select2.full.min.js"></script>

<script>
  $(function() {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>

<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Edit Data Petugas <strong><?php echo $edit->id; ?></strong></h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->

      <?php echo form_open('petugas/edit'); ?>
      <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
      <div class="box-body">

        <?php if (validation_errors() != false) { ?>
          <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <?php echo validation_errors(); ?>
          </div>
        <?php } ?>

        <div class="form-group">
          <label for="id">Id Petugas</label>
          <input type="text" name="id" id="id" class="form-control" id="id" placeholder="Id Petugas" value="<?php echo $edit->id; ?>">
        </div>
        <div class="form-group">
          <label for="username">Username</label>
          <input type="text" name="username" id="username" class="form-control" id="username" placeholder="Username Petugas" value="<?php echo $edit->username; ?>">
        </div>
        <div class="form-group">
          <label for="password">Password</label>
          <input type="password" name="password" id="password" class="form-control" id="password" placeholder="Password Petugas" value="<?php echo $edit->password; ?>">
        </div>
        <div class="form-group">
          <label for="nama">Nama Petugas</label>
          <input type="text" name="nama" id="nama" class="form-control" id="nama" placeholder="Nama Lengkap Petugas" value="<?php echo $edit->nama; ?>">
        </div>
        <div class="form-group">
          <label for="level">Level Petugas</label>
          <?php echo form_dropdown('level', $level, $edit->level); ?>
        </div>

      </div>
      <!-- /.box-body -->

      <div class="box-footer">
        <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
        <button type="submit" class="btn btn-primary" name="submit" value="submit">Update</button>
      </div>
      <?php echo form_close(); ?>
    </div>
    <!-- /.box -->

    <!-- Form Element sizes -->

    <!-- /.box -->


    <!-- /.box -->

    <!-- Input addon -->

    <!-- /.box -->

  </div>
  <!--/.col (left) -->
  <!-- right column -->

  <!--/.col (right) -->
</div>
<!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2/select2.min.css">
  <!-- Select2 -->
<script src="<?=base_url()?>assets/plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>

  <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Siswa <strong><?php echo $edit->id; ?></strong></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open_multipart('siswa/edit'); ?>
              <input type="hidden" name="id" value="<?php echo $edit->id; ?>">
              <div class="box-body">

                <?php if(validation_errors() != false) { ?>
                  <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo validation_errors(); ?>
                </div>
                <?php } ?>

                <div class="form-group">
                  <label for="id">NISN</label>
                  <input type="text" name="id" id="id" class="form-control" placeholder="NISN" value="<?php echo $edit->id; ?>">
                </div>
              <div class="form-group">
                  <label for="nama">Nama Lengkap</label>
                  <input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Lengkap" 
                  value="<?php echo $edit->nama; ?>">
                </div>
              <div class="form-group">
                  <label for="kelas">Kelas</label>
                  <?php echo form_dropdown('kelas', $kelas, $edit->id_kelas); ?>
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat Lengkap</label>
                  <input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat Lengkap" value="<?php echo $edit->alamat; ?>">
                </div>
                <div class="form-group">
                  <label for="telpon">No.Telepon</label>
                  <input type="text" name="telpon" id="telpon" class="form-control" placeholder="No.Telepon" value="<?php echo $edit->telpon; ?>">
                </div>
                <div class="form-group">
                  <label for="id_spp">Nomer SPP</label>
                  <?php echo form_dropdown('spp', $spp, $edit->id); ?>
                </div>
                <div class="form-group">
                  <label for="email">Email Siswa</label>
                  <input type="email" name="email" id="email" class="form-control" placeholder="oktan.christian@gmail.com" value="<?php echo $edit->email; ?>">
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
                <button type="submit" class="btn btn-primary" name="submit" value="submit">Update</button>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
<!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2/select2.min.css">
  <!-- Select2 -->
<script src="<?=base_url()?>assets/plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>

  <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Siswa</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open_multipart('siswa/add'); ?>

              <div class="box-body">

                <?php if(validation_errors() != false) { ?>
                  <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo validation_errors(); ?>
                </div>
                <?php } ?>

                <div class="form-group">
                  <label for="NISN">NISN</label>
                  <input type="text" name="id" id="id" class="form-control" placeholder="NISN" value="<?php echo set_value('id'); ?>">
                </div>
              <div class="form-group">
                  <label for="nama">Nama Lengkap</label>
                  <input type="text" name="nama" id="nama" class="form-control" placeholder="Christian Oktan" 
                  value="<?php echo set_value('tahun'); ?>">
                </div>
              <div class="form-group">
                  <label for="kelas">Kelas</label>
                  <?php echo form_dropdown('kelas', $kelas, 1); ?>
                </div>
                <div class="form-group">
                  <label for="alamat">Alamat Lengkap</label>
                  <input type="text" name="alamat" id="alamat" class="form-control" placeholder="JL. Asri 2 Blok 5 Nob.30" value="<?php echo set_value('alamat'); ?>">
                </div>
                <div class="form-group">
                  <label for="telpon">No.Telepon</label>
                  <input type="text" name="telpon" id="telpon" class="form-control" placeholder="081519252223" value="<?php echo set_value('telpon'); ?>">
                </div>
                <div class="form-group">
                  <label for="spp">No.SPP</label>
                  <?php echo form_dropdown('spp', $spp, 1); ?>
                </div>
                <div class="form-group">
                  <label for="email">Email Siswa</label>
                  <input type="email" name="email" id="email" class="form-control" placeholder="oktan.christian@gmail.com" value="<?php echo set_value('email'); ?>">
                </div>
              </div>
              

              <!-- /.box-body -->

              <div class="box-footer">
              <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
                <button type="submit" class="btn btn-primary" name="submit" value="submit">Save</button>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
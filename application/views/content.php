<style type="text/css">
  .gambar {
    height: 100px !important;
    width: 130px !important;
  }
</style>

<div class="col-md-12">
              <!-- USERS LIST -->
              <div class="box box-default">
                <div class="box-header with-border">
                  <h3 class="box-title">Silahkan Pilih Kelas</h3>

                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <ul class="users-list clearfix">
                    <?php
                  $no = 1;
                  foreach($siswa as $row) {
                  ?>  
                      <li>
                        <a class="users-list-name" href="<?=base_url()?>views/siswa/kelasx">Kelas X</a>
                        <p><?php echo $row->id_kelas; ?></p>
                        <p><?php echo $row->alamat; ?></p>
                        <p><?php echo $row->telpon; ?></p>
                        <p><?php echo $row->id_spp; ?></p>
                      </li>
                  <?php } ?>

                  </ul>
                  <!-- /.users-list -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer text-center">
                  <a href="<?=base_url()?>siswa" class="uppercase">Lihat Semua siswa</a>
                </div>
                <!-- /.box-footer -->
              </div>
              <!--/.box -->
            </div>
<!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2/select2.min.css">
  <!-- Select2 -->
<script src="<?=base_url()?>assets/plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>
<!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datepicker/datepicker3.css">
  <!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

  <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Data Pembayaran</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open('transaksi/add'); ?>

              <div class="box-body">

                <?php if(validation_errors() != false) { ?>
                  <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo validation_errors(); ?>
                </div>
                <?php } ?>
                <div class="form-group">
                  <label for="nisn">NISN</label>
                  <?php echo form_dropdown('nisn', $siswa, 1); ?>
                </div>
                <div class="form-group">
                  <label for="bulan_dibayar">Bulan dibayar</label>
                  <?php echo form_dropdown('bulan_dibayar', $bulan, "Januari"); ?>
                </div>
                <div class="form-group">
                  <label for="spp">No. SPP</label>
                  <?php echo form_dropdown('spp', $spp, 1); ?>
                </div>
                <div class="form-group">
                  <label for="transaksi">Jumlah Bayar</label>
                  <input type="text" name="jml_bayar" id="jml_bayar" class="form-control" placeholder="Jumlah Bayar" 
                  value="<?php echo set_value('jml_bayar'); ?>">
                </div>
              
              
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
                <button type="submit" class="btn btn-primary" name="submit" value="submit">Bayar</button>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
<!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/select2/select2.min.css">
  <!-- Select2 -->
<script src="<?=base_url()?>assets/plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".select2").select2();
  });
</script>
<!-- Select2 -->
  <link rel="stylesheet" href="<?=base_url()?>assets/plugins/datepicker/datepicker3.css">
  <!-- bootstrap datepicker -->
<script src="<?=base_url()?>assets/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
     //Date picker
    $('#tglsewa').datepicker({ autoclose: true, dateFormat: 'yy-mm-dd' });
    $('#tglkembali').datepicker({ autoclose: true, dateFormat: 'yy-mm-dd' });
      //dateFormat: 'yy-mm-dd'
//}{
     //// autoclose: true,
     // dateFormat: 'yy-mm-dd'
    //});
  });
</script>
  <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Data Transaksi <?php echo $edit->id_pembayaran; ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

           <?php echo form_open('transaksi/edit'); ?>
           <input type="hidden" name="id" value="<?php echo $edit->id_pembayaran; ?>">

              <div class="box-body">

                <?php if(validation_errors() != false) { ?>
                  <div class="alert alert-danger alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <?php echo validation_errors(); ?>
                </div>
                <?php } ?>
                <div class="form-group">
                  <label for="id_pembayaran">No. Pembayaran</label>
                  <input type="text" name="id_pembayaran" id="id_pembayaran" class="form-control" placeholder="No. Pembayaran" disabled
                  value="<?php echo $edit->id_pembayaran; ?>">
                </div>
                <div class="form-group">
                  <label for="nisn">NISN</label>
                  <?php echo form_dropdown('nisn', $siswa, $edit->nisn); ?>
                </div>
                <div class="form-group">
                  <label for="bulan_dibayar">Bulan dibayar</label>
                  <?php echo form_dropdown('bulan_dibayar', $bulan, $edit->bulan_dibayar); ?>
                </div>
                <div class="form-group">
                  <label for="spp">No. SPP</label>
                  <?php echo form_dropdown('spp', $spp, $edit->id_spp); ?>
                </div>
                <div class="form-group">
                  <label for="jml_bayar">Jumlah Tagihan</label>
                  <input type="text" name="jml_bayar" id="jml_bayar" class="form-control" placeholder="Jumlah Bayar" 
                  value="<?php echo $edit->jml_bayar; ?>">
                </div>                                                                    
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
              <button type="button" class="btn btn-default" onclick="window.history.back()">Cancel</button>
                <button type="submit" class="btn btn-primary" name="submit" value="submit">Update</button>
              </div>
            <?php echo form_close(); ?>
          </div>
          <!-- /.box -->

          <!-- Form Element sizes -->
          
          <!-- /.box -->

          
          <!-- /.box -->

          <!-- Input addon -->
          
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
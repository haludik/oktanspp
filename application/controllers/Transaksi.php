<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('transaksimodel');
		$this->load->model('siswamodel');
		$this->load->model('sppmodel');
		$this->load->model('petugasmodel');

		//cek apakah sudah login atau belum
		if ($this->session->userdata('status') != "login") {
			$this->session->set_flashdata('info', 'Maaf, Anda harus login terlebih dahulu.');
			redirect(base_url("site"));
		}
	}

	public function index()
	{
		$data['transaksi'] = $this->transaksimodel->Gettransaksi();
		$data['content'] = "transaksi/view";
		$this->load->view('template/template', $data);
	}

	public function add()
	{
		$siswa_array = [];
		$dropdownsiswa = $this->siswamodel->GetSiswaDropdown();
		foreach ($dropdownsiswa as $siswa) {
			$siswa_array[$siswa['id']] = $siswa['id'] . " | " .  $siswa['nama'];
		}

		$spp_array = [];
		$dropdownspp = $this->sppmodel->GetSppDropdown();
		foreach ($dropdownspp as $spp) {
			$spp_array[$spp['id']] = $spp['id'] . " | Tahun: " . $spp['tahun'] . " | Total: Rp. " . $spp['nominal'];
		}

		
		$data['bulan'] = ['Januari' => 'Januari', 'Februari' => 'Februari', 'Maret' => 'Maret', 'April' => 'April', 'Mei' => 'Mei', 'Juni' => 'Juni', 'Juli' => 'Juli', 'Agustus' => 'Agustus', 'September' => 'September', 'Oktober' => 'Oktober', 'November' => 'November', 'Desember' => 'Desember'];

		$data['siswa'] = $siswa_array;
		$data['spp'] = $spp_array;

		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('id_pembayaran', 'Field id_pembayaran');
			$this->form_validation->set_rules('id_petugas', 'Field id_petugas');
			$this->form_validation->set_rules('nisn', 'Field nisn');
			$this->form_validation->set_rules('bulan_dibayar', 'Field bulan_dibayar', 'required');
			$this->form_validation->set_rules('id_spp', 'Field id_spp');
			$this->form_validation->set_rules('jml_bayar', 'Field jml_bayar', 'required');

			if ($this->form_validation->run() == FALSE) {
				$data['content'] = "transaksi/add";
				$this->load->view('template/template', $data);
			} else {
				$insert = $this->transaksimodel->insert();
				if ($insert) {
					$this->session->set_flashdata('info', 'Data berhasil disimpan');

					redirect('transaksi');
				}
			}
		} else {
			$id = $this->uri->segment(3);

			$data['content'] = "transaksi/add";
			$this->load->view('template/template', $data);
		}
	}

	public function edit()
	{
		
		$dropdownsiswa = $this->siswamodel->GetSiswaDropdown();
		foreach ($dropdownsiswa as $siswa) {
			$siswa_array[$siswa['id']] = $siswa['id'] . " | " .  $siswa['nama'];
		}
		$dropdownspp = $this->sppmodel->GetSppDropdown();
		foreach ($dropdownspp as $spp) {
			$spp_array[$spp['id']] = $spp['id'] . " | Tahun: " . $spp['tahun'] . " | Total: Rp. " . $spp['nominal'];
		}

		$data['bulan'] = ['Januari' => 'Januari', 'Februari' => 'Februari', 'Maret' => 'Maret', 'April' => 'April', 'Mei' => 'Mei', 'Juni' => 'Juni', 'Juli' => 'Juli', 'Agustus' => 'Agustus', 'September' => 'September', 'Oktober' => 'Oktober', 'November' => 'November', 'Desember' => 'Desember'];

		$data['siswa'] = $siswa_array;
		$data['spp'] = $spp_array;

		if ($this->input->post('submit')) {
			$id_pembayaran 	= $this->input->post('id');
			$id_petugas 	= $_SESSION['id_petugas'];
			$nisn 			= $this->input->post('nisn');
			$bulan_dibayar 	= $this->input->post('bulan_dibayar');
			$id_spp			= $this->input->post('spp');
			$jml_bayar 		= $this->input->post('jml_bayar');

			$field = array(
				'id_pembayaran' 	=> $id_pembayaran,
				'id_petugas' 		=> $id_petugas,
				'nisn'  			=> $nisn,
				'bulan_dibayar' 	=> $bulan_dibayar,
				'id_spp'  			=> $id_spp,
				'jml_bayar'  		=> $jml_bayar,
			);

			$this->db->where('id_pembayaran', $id_pembayaran);
			$this->db->update('pembayaran', $field);

			if ($this->db->affected_rows()) {
				$this->session->set_flashdata('info', 'Data berhasil diupdate');
				redirect('transaksi');
			} else {
				$this->session->set_flashdata('info', 'Data gagal diupdate');
				redirect('transaksi');
			}
		} else {
			$id = $this->uri->segment(3);

			$data['edit'] = $this->transaksimodel->GetData($id);
			$data['list'] = $this->transaksimodel->Gettransaksi();
			$data['content'] = "transaksi/edit";
			$this->load->view('template/template', $data);
		}
	}

	public function hapus($id)
	{
		$this->transaksimodel->delete($id);

		if ($this->db->affected_rows()) {
			$this->session->set_flashdata('info', 'Data berhasil dihapus');
			redirect('transaksi');
		} else {
			$this->session->set_flashdata('info', 'Data gagal dihapus');
			redirect('transaksi');
		}
	}
}

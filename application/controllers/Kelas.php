<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('kelasmodel');

		//cek apakah sudah login atau belum
		if ($this->session->userdata('status') != "login") {
			$this->session->set_flashdata('info', 'Maaf, Anda harus login terlebih dahulu.');
			redirect(base_url("site"));
		}
	}

	public function index()
	{
		$data['kelas'] = $this->kelasmodel->GetKelas();
		$data['content'] = "kelas/view";
		$this->load->view('template/template', $data);
	}

	public function add()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('nama', 'Field of Kelas', 'trim|required|max_length[12]');
			$this->form_validation->set_rules('jurusan', 'Field of Jurusan', 'trim|required|min_length[3]');
			if ($this->form_validation->run() == FALSE) {
				$data['content'] = "kelas/add";
				$this->load->view('template/template', $data);
			} else {
				$insert = $this->kelasmodel->insert();
				if ($insert) {
					$this->session->set_flashdata('info', 'Data berhasil disimpan');
					redirect('kelas');
				}
			}
		} else {
			$data['content'] = "kelas/add";
			$this->load->view('template/template', $data);
		}
	}

	public function edit($id = null)
	{
		if ($this->input->post('submit')) {
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$jurusan = $this->input->post('jurusan');

			$field = array(
				'id' => $id,
				'nama' => $nama,
				'jurusan'  => $jurusan,
			);


			$this->db->where('id', $id);
			$this->db->update('kelas', $field);

			if ($this->db->affected_rows()) {
				$this->session->set_flashdata('info', 'Data berhasil diupdate');
			} else {
				$this->session->set_flashdata('info', 'Data gagal diupdate');
			}

			redirect('kelas');
		} else {
			$data['edit'] = $this->kelasmodel->GetData($id);
			$data['content'] = "kelas/edit";
			$this->load->view('template/template', $data);
		}
	}

	public function hapus($id)
	{
		$this->load->model('siswamodel');
		$usedKelas = $this->siswamodel->GetDataByKelas($id)->num_rows();
		if ($usedKelas > 0) {
			$this->session->set_flashdata('info', 'Data gagal dihapus. Masih terdapat siswa di dalam kelas ini.');
		} else {
			$delete = $this->kelasmodel->delete($id);

			if ($delete && $this->db->affected_rows()) {
				$this->session->set_flashdata('info', 'Data berhasil dihapus');
			} else {
				$this->session->set_flashdata('info', 'Data gagal dihapus');
			}
		}

		redirect('kelas');
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct(){
		parent::__construct();
		$this->load->model('siswamodel');
		$this->load->model('transaksimodel');
	}

	public function index()
	{
		$this->load->view('history/cari');
		
	}

	function cari(){
		$nisn = $this->input->post('nisn');
		$nama = $this->input->post('nama');
	
		$siswa =  $this->siswamodel->GetDataByNIKNama($nisn, $nama);
		$rowsiswa = $siswa->row();
		$cek = $siswa->num_rows();
		if($cek > 0){

			$data_session = array(
				'nisn' => $nisn,
				'namasiswa' => $rowsiswa->nama,
				'kelas' => $rowsiswa->kelas,
				'jurusan' => $rowsiswa->jurusan
				);

			$this->session->set_userdata($data_session);

			redirect(base_url("history/tampil"));

		}else{
			$this->session->set_flashdata('info', 'NISN / Nama tidak dapat ditemukan');
			redirect('history');
			echo "NISN / Nama tidak dapat ditemukan";
		}
	}

	function tampil()
	{
		$data['transaksi'] = $this->transaksimodel->GetTransaksiByNISN($this->session->userdata('nisn'));
		$data['content'] = "transaksi/view";
		$this->load->view('history/view', $data);
	}

	function back(){
		$this->session->sess_destroy();
		redirect(base_url('history'));
	}
}

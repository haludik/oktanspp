<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Spp extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('sppmodel');

		//cek apakah sudah login atau belum
		if($this->session->userdata('status') != "login"){
			$this->session->set_flashdata('info', 'Maaf, Anda harus login terlebih dahulu.');
			redirect(base_url("site"));
		}
	}

	public function index()
	{	
		$data['spp'] = $this->sppmodel->GetSpp();
		$data['content'] = "spp/view";
		$this->load->view('template/template', $data);
	}

	public function add()
	{

		if($this->input->post('submit')) 
			{
				$this->form_validation->set_rules('id', 'Field of id',);
				$this->form_validation->set_rules('tahun', 'Field of tahun', 'trim');
				$this->form_validation->set_rules('nominal', 'Field of nominal', 'trim');

				if ($this->form_validation->run() == FALSE) {
							$data['content'] = "spp/add";
							$this->load->view('template/template', $data);
		                } else {	
					                $insert = $this->sppmodel->insert();
				                    if($insert) 
										{
											$this->session->set_flashdata('info', 'Data berhasil disimpan');
											redirect('spp');
										}
			                    
		                }

			} else {
				$data['content'] = "spp/add";
				$this->load->view('template/template', $data);
			}

		
	
	}

	public function edit($id = null)
	{	
		if($this->input->post('submit')) 
			{
				$id 		= $this->input->post('id');
				$tahun 		= $this->input->post('tahun');
				$nominal 	= $this->input->post('nominal');

						$field = array (
							'id' 			=> $id,
						    'tahun' 		=> $tahun,
						    'nominal'  		=> $nominal,
						);

				$this->db->where('id', $id);
				$this->db->update('spp', $field);

				if($this->db->affected_rows()) {
					$this->session->set_flashdata('info', 'Data berhasil diupdate');
					redirect('spp');
				} else {
					$this->session->set_flashdata('info', 'Data gagal diupdate');
					redirect('spp');
				}

			} else {
				$id = $this->uri->segment(3);
				$data['edit'] = $this->sppmodel->GetData($id);
				$data['content'] = "spp/edit";
				$this->load->view('template/template', $data);
			}

	}

	public function hapus($id)
	{
		$this->load->model('siswamodel');
		$usedSpp = $this->siswamodel->GetDataBySpp($id)->num_rows();
		if ($usedSpp > 0) {
			$this->session->set_flashdata('info', 'Data gagal dihapus. Masih terdapat spp siswa di dalam kelas ini.');
		} else {
			$delete = $this->sppmodel->delete($id);

			if ($delete && $this->db->affected_rows()) {
				$this->session->set_flashdata('info', 'Data berhasil dihapus');
			} else {
				$this->session->set_flashdata('info', 'Data gagal dihapus');
			}
		}

		redirect('spp');
	}

}

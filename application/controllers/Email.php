<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {

     public function __construct()
     {
          parent::__construct();
          $this->load->library('form_validation');
          $this->load->library('session');
          $this->load->model('siswamodel');
          $this->load->model('kelasmodel');
          $this->load->model('sppmodel');
          $this->load->helper(array('form', 'url'));
     }

     public function index()
     {
          $data['siswa'] = $this->siswamodel->GetSiswa();
     }

     public function send()
     {
          $config['mailtype'] = 'text';
          $config['protocol'] = 'smtp';
          $config['smtp_host'] = 'smtp.mailtrap.io';
          $config['smtp_user'] = '0798f098eaacbb';
          $config['smtp_pass'] = 'b4624118bd2279';
          $config['smtp_port'] = 465;
          $config['newline'] = "\r\n";

          $this->load->library('email', $config);
          $this->email->from('no-reply@smkangkasa1.com', 'smkangkasa1margahayu.com');
          $this->email->to('oktan.christian@gmail.com');
          $this->email->subject('Tagihan Anda Belum Dibayar');
          $this->email->message('Silahkan Membayar Ke Bendahara Sekolah/Melalui Bank Terdekat!!! 
Dengan Melalui Link berikut: 
https://bahasaweb.com/tutorial-cara-kirim-email-dengan-codeigniter/');

          if($this->email->send()) {
               echo 'Email berhasil dikirim';
               echo '<a href="../transaksi">Click here</a>';
          }
          else {
               echo 'Email tidak berhasil dikirim';
               echo '<br />';
               echo $this->email->print_debugger();
          }

     }

}
?>
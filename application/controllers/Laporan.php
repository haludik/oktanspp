<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('transaksimodel');
		$this->load->model('siswamodel');
		$this->load->model('kelasmodel');
		$this->load->model('sppmodel');
		$this->load->model('petugasmodel');

		//cek apakah sudah login atau belum
		if ($this->session->userdata('status') != "login") {
			$this->session->set_flashdata('info', 'Maaf, Anda harus login terlebih dahulu.');
			redirect(base_url("site"));
		}
	}

	public function index()
	{
		$siswa_array = [];
		$dropdownsiswa = $this->siswamodel->GetSiswaDropdown();
		foreach ($dropdownsiswa as $siswa) {
			$siswa_array[$siswa['id']] = $siswa['id'];
		}

		$kelas_array = [];
		$dropdownkelas = $this->kelasmodel->GetKelasOnlyDropdown();
		foreach ($dropdownkelas as $kelas) {
			$kelas_array[$kelas['kelas']] = $kelas['kelas'];
		}

		
		$jurusan_array = [];
		$dropdownjurusan = $this->kelasmodel->GetJurusanDropdown();
		foreach ($dropdownjurusan as $jurusan) {
			$jurusan_array[$jurusan['jurusan']] = $jurusan['jurusan'];
		}

		$tahun_array = [];
		$dropdowntahun = $this->sppmodel->GetSppTahunDropdown();
		foreach ($dropdowntahun as $tahun) {
			$tahun_array[$tahun['tahun']] = $tahun['tahun'];
		}

		$petugas_array = [];
		$dropdownpetugas = $this->petugasmodel->GetPetugasDropdown();
		foreach ($dropdownpetugas as $petugas) {
			$petugas_array[$petugas['id']] = $petugas['username'];
		}

		$data['tahun'] = $tahun_array;
		$data['bulan'] = ['Januari' => 'Januari', 'Februari' => 'Februari', 'Maret' => 'Maret', 'April' => 'April', 'Mei' => 'Mei', 'Juni' => 'Juni', 'Juli' => 'Juli', 'Agustus' => 'Agustus', 'September' => 'September', 'Oktober' => 'Oktober', 'November' => 'November', 'Desember' => 'Desember'];
		$data['status'] = ['Belum Lunas' => 'Belum Lunas', 'Lunas' => 'Lunas'];
		$data['siswa'] = $siswa_array;
		$data['kelas'] = $kelas_array;
		$data['jurusan'] = $jurusan_array;
		$data['petugas'] = $petugas_array;
		$data['content'] = "laporan/view";
		$this->load->view('template/template', $data);
	}

	function generate()
	{
		$nisn = $this->input->post('nisn');
		$nama = $this->input->post('nama');
		$kelas = $this->input->post('kelas');
		$jurusan = $this->input->post('jurusan');
		$tahun = $this->input->post('tahun');
		$bulan_dibayar = $this->input->post('bulan_dibayar');
		$petugas = $this->input->post('petugas');
		$status = $this->input->post('status');


		$dataLaporan =  $this->transaksimodel->GetLaporan($nisn, $nama, $kelas, $jurusan, $tahun, $bulan_dibayar, $petugas, $status);

		$data['nisn'] = ($nisn == "")? "Tidak digunakan untuk filter" : $nisn;
		$data['nama'] = ($nama == "")? "Tidak digunakan untuk filter" : $nama;
		$data['kelas'] = ($kelas == "- Semua Kelas -")? "Semua Kelas" : $kelas;
		$data['jurusan'] = ($jurusan == "- Semua Jurusan -")? "Semua Jurusan" : $jurusan;
		$data['tahun'] = ($tahun == "- Semua Tahun -")? "Semua Tahun" : $tahun;
		$data['bulan_dibayar'] = ($bulan_dibayar == "- Semua Bulan -")? "Semua Bulan" : $bulan_dibayar;
		$data['petugas'] = ($petugas == "- Semua Petugas -")? "Semua Petugas" : $petugas;
		$data['status'] = ($status == "- Semua Status -")? "Semua Status" : $status;
		$data['hasdata'] = true;
		if ($dataLaporan->num_rows() > 0) {
			$data['laporan'] = $dataLaporan->result();
		
		} else {
			$data['hasdata'] = false;
		}

		$this->load->view('laporan/print', $data);
	}
}

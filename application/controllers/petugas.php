<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Petugas extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('petugasmodel');

		//cek apakah sudah login atau belum
		if ($this->session->userdata('status') != "login") {
			$this->session->set_flashdata('info', 'Maaf, Anda harus login terlebih dahulu.');
			redirect(base_url("site"));
		}
	}

	public function index()
	{
		$data['petugas'] = $this->petugasmodel->GetPetugas();
		$data['content'] = "petugas/view";
		$this->load->view('template/template', $data);
	}

	public function add()
	{
		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('id', 'Field of id','trim|max_length[10]');
			$this->form_validation->set_rules('username', 'Field of username', 'trim|required|max_length[20]');
			$this->form_validation->set_rules('password', 'Field of password', 'trim|required|max_length[15]');
			$this->form_validation->set_rules('nama', 'Field of nama', 'trim|required|max_length[30]');
			$this->form_validation->set_rules('level', 'Field of level', 'trim|required');

			if (!$this->form_validation->run()) {
				$data['content'] = "petugas/add";
				$this->load->view('template/template', $data);
			} else {
				$insert = $this->petugasmodel->insert();
				if ($insert) {
					$this->session->set_flashdata('info', 'Data berhasil disimpan');
					redirect('petugas');
				}
			}
		} else {
			$data['level'] = ['Admin' => 'Admin', 'Petugas' => 'Petugas'];
			$data['content'] = "petugas/add";
			$this->load->view('template/template', $data);
		}
	}

	public function edit($id = null)
	{
		if ($this->input->post('submit')) {
			$id 		= $this->input->post('id');
			$username 	= $this->input->post('username');
			$password 	= $this->input->post('password');
			$nama 		= $this->input->post('nama');
			$level	 	= $this->input->post('level');

			$field = array(
				'id' 			=> $id,
				'username'  	=> $username,
				'password' 		=> md5($password),
				'nama'  		=> $nama,
				'level'  		=> $level,
			);

			$this->db->where('id', $id);
			$this->db->update('petugas', $field);

			if ($this->db->affected_rows()) {
				$this->session->set_flashdata('info', 'Data berhasil diupdate');
				redirect('petugas');
			} else {
				$this->session->set_flashdata('info', 'Data gagal diupdate');
				redirect('petugas');
			}
		} else {

			$data['level'] = ['Admin' => 'Admin', 'Petugas' => 'Petugas'];
			$data['edit'] = $this->petugasmodel->GetData($id);
			$data['content'] = "petugas/edit";
			$this->load->view('template/template', $data);
		}
	}

	public function hapus($id)
	{
		$this->petugasmodel->delete($id);

		if ($this->db->affected_rows()) {
			$this->session->set_flashdata('info', 'Data berhasil dihapus');
			redirect('petugas');
		} else {
			$this->session->set_flashdata('info', 'Data gagal dihapus');
			redirect('petugas');
		}
	}
}

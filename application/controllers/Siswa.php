<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->model('siswamodel');
		$this->load->model('kelasmodel');
		$this->load->model('sppmodel');


		//cek apakah sudah login atau belum
		if ($this->session->userdata('status') != "login") {
			$this->session->set_flashdata('info', 'Maaf, Anda harus login terlebih dahulu.');
			redirect(base_url("site"));
		}
	}

	public function index()
	{
		$data['siswa'] = $this->siswamodel->GetSiswa();
		$data['content'] = "siswa/view";
		$this->load->view('template/template', $data);
	}

	public function add()
	{
		$kelas_array = [];
		$dropdownkelas = $this->kelasmodel->GetKelasDropdown();
		foreach ($dropdownkelas as $kelas) {
			$kelas_array[$kelas['id']] = $kelas['nama'];
		}
		$spp_array = [];
		$dropdownspp = $this->sppmodel->GetSppDropdown();
		foreach ($dropdownspp as $spp) {
			$spp_array[$spp['id']] = $spp['id'];
		}

		$data['kelas'] = $kelas_array;
		$data['spp'] = $spp_array;

		if ($this->input->post('submit')) {
			$this->form_validation->set_rules('id', 'Field of nisn', 'trim|required|min_length[5]|max_length[15]');
			$this->form_validation->set_rules('nama', 'Field of nama', 'trim|required|min_length[5]|max_length[30]');
			$this->form_validation->set_rules('kelas', 'Field of kelas');
			$this->form_validation->set_rules('alamat', 'Field of alamat', 'trim|required|min_length[5]|max_length[35]');
			$this->form_validation->set_rules('telpon', 'Field of telpon', 'trim|required|min_length[5]|max_length[14]');
			$this->form_validation->set_rules('id_spp', 'Field of spp');
			$this->form_validation->set_rules('email', 'Field of email','trim|min_length[5]|max_length[20]');

			if (!$this->form_validation->run()) {
				$data['content'] = "siswa/add";
				$this->load->view('template/template', $data);
			} else {
				$insert = $this->siswamodel->insert();
				if ($insert) {
					$this->session->set_flashdata('info', 'Data berhasil disimpan');
					redirect('siswa');
				}
			}
		} else {
		
		
			$data['list'] = $this->siswamodel->getSiswa();
			$data['content'] = "siswa/add";
			$this->load->view('template/template', $data);
		}
	}

	public function edit()
	{

		$kelas_array = [];
		$dropdownkelas = $this->kelasmodel->GetKelasDropdown();
		foreach ($dropdownkelas as $kelas) {
				$kelas_array[$kelas['id']] = $kelas['nama'] . " " . $kelas['jurusan']; 
		}

		$spp_array = [];
		$dropdownspp = $this->sppmodel->GetSppDropdown();
		foreach ($dropdownspp as $spp) {
			$spp_array[$spp['id']] = $spp['tahun'];
		}

		if ($this->input->post('submit')) {
			$id 		= $this->input->post('id');
			$nama 		= $this->input->post('nama');
			$id_kelas 	= $this->input->post('kelas');
			$alamat 	= $this->input->post('alamat');
			$telpon 	= $this->input->post('telpon');
			$spp 		= $this->input->post('spp');
			$email 		= $this->input->post('email');

			$field = array(
				'id' 			=> $id,
				'nama'  		=> $nama,
				'id_kelas' 		=> $id_kelas,
				'alamat'  		=> $alamat,
				'telpon'  		=> $telpon,
				'id_spp'  		=> $spp,
				'email'  		=> $email
			);

			$this->db->where('id', $id);
			$this->db->update('siswa', $field);

			if ($this->db->affected_rows()) {
				$this->session->set_flashdata('info', 'Data berhasil diupdate');
				redirect('siswa');
			} else {
				$this->session->set_flashdata('info', 'Data gagal diupdate');
				redirect('siswa');
			}
		} else {
			$id = $this->uri->segment(3);

			$data['kelas'] = $kelas_array;
			$data['spp'] = $spp_array;

			$data['edit'] = $this->siswamodel->GetData($id);
			$data['list'] = $this->siswamodel->getSiswa();
			$data['content'] = "siswa/edit";
			$this->load->view('template/template', $data);
		}
	}

	public function hapus($id)
	{
		$this->siswamodel->delete($id);

		if ($this->db->affected_rows()) {
			$this->session->set_flashdata('info', 'Data berhasil dihapus');
			redirect('siswa');
		} else {
			$this->session->set_flashdata('info', 'Data gagal dihapus');
			redirect('siswa');
		}
	}
}

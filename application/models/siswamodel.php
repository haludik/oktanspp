<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswamodel extends CI_Model {


	function __construct() {
		parent::__construct();
		$this->load->library('fungsi');
	}

	function GetSiswa() {
		$this->db->select('siswa.*, kelas.nama AS kelas, kelas.jurusan');
		$this->db->from("siswa");
		$this->db->join('kelas', 'kelas.id = siswa.id_kelas');
		$this->db->order_by("nama", "id_kelas");
		$query = $this->db->get(); 
		return $query->result();
    }

    function GetData($id) {
    	$id = $this->uri->segment(3);
    	return $this->db->get_where('siswa', array('id'=> $id))->row();
	}
	
	function GetDataByKelas($id) {
    	return $this->db->get_where('siswa', array('id_kelas'=> $id));
    }

	function GetDataBySpp($id) {
    	return $this->db->get_where('siswa', array('id_spp'=> $id));
	}
	

	function GetDataByNIKNama($nisn, $nama) {
		$this->db->select('siswa.*, kelas.nama AS kelas, kelas.jurusan');
		$this->db->join('kelas', 'kelas.id = siswa.id_kelas');
    	return $this->db->get_where('siswa', array('siswa.id'=> $nisn, 'siswa.nama'=> $nama));
    }

    function GetSiswaDropdown() {
	$this->db->select('id, nama');
	$result = $this->db->get('siswa')->result_array();
    return $result;
    }


    public function getKelas(){
  		$this->db->from("kelas");
		$this->db->order_by("nama", "jurusan");
		$query = $this->db->get(); 
		return $query->result();

 	}

 	public function insert()
	{
		$id 		= $this->input->post('id');
		$nama 		= $this->input->post('nama');
		$id_kelas 	= $this->input->post('kelas');
		$alamat		= $this->input->post('alamat');
		$telpon 	= $this->input->post('telpon');
		$id_spp 	= $this->input->post('spp');
		$email	 	= $this->input->post('email');
		

		$input = array (
		    'id' 			=> $id,
		    'nama'  		=> $nama,
		    'id_kelas' 		=> $id_kelas,
		    'alamat'  		=> $alamat,
		    'telpon'  		=> $telpon,
		    'id_spp'  		=> $id_spp,
		    'email'  		=> $email
		);

		return $this->db->insert('siswa', $input);
	}

	public function delete($param) {
		return $this->db->delete('siswa', array('id' => $param));
	}

}

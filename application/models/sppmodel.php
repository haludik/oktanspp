<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sppmodel extends CI_Model {


	function __construct() {
		parent::__construct();
		$this->load->library('fungsi');
	}

	function Getspp() {
        return $this->db->get('spp')->result();
    }

    function GetSppDropdown() {
	$this->db->select('id, tahun, nominal');
	$result = $this->db->get('spp')->result_array();
    return $result;
	}
	
	function GetSppTahunDropdown() {
		$this->db->distinct();
		$this->db->select('tahun');
		$result = $this->db->get('spp')->result_array();
		return $result;
		}

    function GetData($id) {
    	$id = $this->uri->segment(3);
    	return $this->db->get_where('spp', array('id'=> $id))->row();
    }

    function GetDataBySpp($id) {
    	return $this->db->get_where('spp', array('id'=> $id));
    }

 	public function insert()
	{
		
		$id 		= $this->input->post('id');
		$tahun 		= $this->input->post('tahun');
		$nominal 	= $this->input->post('nominal');

		$input = array (
			'id' 			=> $id,
			'tahun' 		=> $tahun,
		    'nominal' 		=> $nominal,
		);

		return $this->db->insert('spp', $input);
	}

	public function delete($param) {
		return $this->db->delete('spp', array('id' => $param));
	}

}

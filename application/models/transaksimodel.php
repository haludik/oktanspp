<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksimodel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->library('fungsi');
	}

	function Gettransaksi() {
		$this->db->select('pembayaran.*, spp.tahun');
		$this->db->from("pembayaran");
		$this->db->join('spp', 'spp.id = pembayaran.id_spp');
		$query = $this->db->get(); ;

        return 	$query->result();
	}

	function GetLaporan($nisn, $nama, $kelas, $jurusan, $tahun, $bulan_dibayar, $petugas, $status) {
		
		if($nisn != "")
		{
			$this->db->like('nisn', $nisn);
		}

		if($nama != "")
		{
			$this->db->like('nama', $nama);
		}

		if($kelas != "- Semua Kelas -")
		{
			$this->db->where('kelas', $kelas);
		}

		if($jurusan != "- Semua Jurusan -")
		{
			$this->db->where('jurusan', $jurusan);
		}

		if($tahun != "- Semua Tahun -")
		{
			$this->db->where('tahun', $tahun);
		}

		if($bulan_dibayar != "- Semua Bulan -")
		{
			$this->db->where('bulan', $bulan_dibayar);
		}
		
		if($petugas != "- Semua Petugas -")
		{
			$this->db->where('IdPetugas', $petugas);
		}

		
		if($status != "- Semua Status -")
		{
			$this->db->where('Status', $status);
		}
		
    	return $this->db->get('laporan');
    }
	
   function GetTransaksiByNISN($nisn) {
        return $this->db->get_where('pembayaran', array('nisn'=> $nisn))->result();
    }

    function GetData($id) {
    	$id = $this->uri->segment(3);
    	return $this->db->get_where('pembayaran', array('id_pembayaran'=> $id))->row();
    }

    function GetDataByPetugas($id) {
    	return $this->db->get_where('pembayaran', array('id_petugas'=> $id));
    }

    function GetDataBySiswa($id) {
    	return $this->db->get_where('pembayaran', array('nisn'=> $id));
    }

    function GetDataByKelas($id) {
    	return $this->db->get_where('pembayaran', array('id_kelas'=> $id));
    }

 	public function insert()
	{
		$id_petugas 		= $_SESSION['id_petugas'];
		$nisn 				= $this->input->post('nisn');
		$bulan_dibayar		= $this->input->post('bulan_dibayar');
		$id_spp 			= $this->input->post('spp');
		$jml_bayar			= $this->input->post('jml_bayar');

		print_r($this->input->post());

		$input = array (
			'id_petugas' 			=> $id_petugas,
		    'nisn' 					=> $nisn,
		    'bulan_dibayar' 		=> $bulan_dibayar,
		    'id_spp'  				=> $id_spp,
		    'jml_bayar'  			=> $jml_bayar
		);

		return $this->db->insert('pembayaran', $input);
	}

	public function delete($param) {
		return $this->db->delete('pembayaran', array('id_pembayaran' => $param));
	}
}
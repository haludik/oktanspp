<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelasmodel extends CI_Model
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('fungsi');
	}

	function GetKelas()
	{
		return $this->db->get('kelas')->result();
	}

	function GetKelasDropdown()
	{
		$this->db->select('id, nama, jurusan');
		$result = $this->db->get('kelas')->result_array();
		return $result;
	}


	function GetKelasOnlyDropdown()
	{
		$this->db->distinct();
		$this->db->select('nama as kelas');
		$result = $this->db->get('kelas')->result_array();
		return $result;
	}

	
	function GetJurusanDropdown()
	{
		$this->db->distinct();
		$this->db->select('jurusan');
		$result = $this->db->get('kelas')->result_array();
		return $result;
	}


	function GetData($id)
	{
		return $this->db->get_where('kelas', array('id' => $id))->row();
	}

	function GetDataByKelas($id)
	{
		return $this->db->get_where('kelas', array('id' => $id));
	}

	public function insert()
	{

		$kelas 		= $this->input->post('nama');
		$jurusan  	= $this->input->post('jurusan');

		$input = array(
			'nama' 		=> $kelas,
			'jurusan'   => $jurusan
		);

		return $this->db->insert('kelas', $input);
	}

	public function delete($param)
	{
		return $this->db->delete('kelas', array('id' => $param));
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugasmodel extends CI_Model {

	function __construct() {
		parent::__construct();
		$this->load->library('fungsi');
	}

	function GetPetugas() {
        return $this->db->get('petugas')->result();
	}
	
	
	function GetPetugasDropdown()
	{
		$this->db->select('id, username, nama');
		$result = $this->db->get('petugas')->result_array();
		return $result;
	}


    function GetData($id) {
    	$id = $this->uri->segment(3);
    	return $this->db->get_where('petugas', array('id'=> $id))->row();
    }

	public function insert()
	{
		
		$id 		= $this->input->post('id');
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$nama		= $this->input->post('nama');
		$level		= $this->input->post('level');

		$input = array (
		    'id' 			=> $id,
		    'username'  	=> $username,
		    'password' 		=> md5($password),
		    'nama'  		=> $nama,
		    'level'  		=> $level,
		);

		return $this->db->insert('petugas', $input);
	}

	public function delete($param) {
		return $this->db->delete('petugas', array('id' => $param));
	}

}
